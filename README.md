Git Scripts 
===

Post-Update
---
This scripts advances the repository's working branch to the lastest push if the workign branch is the same as the master branch.  This goes into the dev server's hook directory, and it enables us to push changes to master on dev and see the changes reflected on the web server.

Pre-Receive
---
This script checks php files for syntax errors and rejects and pushes that would introduce an error.  It executes 'php -l' on all .php files.